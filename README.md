Gestor de actividades
=====================

Esta aplicación es una versión modificada del gestor de actividades del 
Consejo de Estudiantes de la Escuela Politécnica Superior
[CEEPS](http://consejo-eps.uco.es), y se ha usado para hacer un taller 
sobre Symfony2.

La herramienta permite crear cuentas y actividades, y permite a los usuarios
y a los administradores gestionar las inscripciones.

Es una herramienta muy simple, ciertamente, pero para mostrar en menos de 
cuatro horas lo que se puede empezar a hacer con Symfony2 es suficiente.
