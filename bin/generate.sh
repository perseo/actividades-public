#! /bin/bash

CONSOLE='php app/console'

if [ ! -e app/console ]
then
    echo Tienes que estar en el directorio principal para ejecutar $(basename $0)
    exit 1
fi

$CONSOLE doctrine:generate:entity --entity="CoreBundle:Activity" --fields="\
    name:string(120) \
    slug:string(120) \
    description:text \
    url:string(255) \
    created_at:datetime \
    updated_at:datetime \
    starts_at:datetime \
    ends_at:datetime \
    registration_starts_at:datetime \
    registration_ends_at:datetime \
    price:float \
    deposit:float \
    seats:smallint \
    enrollments:string(120) \
    " --format=annotation --with-repository --no-interaction

$CONSOLE doctrine:generate:entity --entity="CoreBundle:Degree" --fields="\
    name:string(120) \
    " --format=annotation --with-repository --no-interaction

$CONSOLE doctrine:generate:entity --entity="CoreBundle:Role" --fields="\
    name:string(120) \
    " --format=annotation --with-repository --no-interaction

$CONSOLE doctrine:generate:entity --entity="CoreBundle:User" --fields="\
    username:string(120) \
    password:string(120) \
    salt:string(120) \
    name:string(120) \
    lastname:string(120) \
    phonenumber:string(50) \
    email:string(120) \
    degree:string(120) \
    year:smallint \
    userRoles:string(120) \
    enrollments:string(120) \
    created_at:datetime \
    lastlogon:datetime \
    " --format=annotation --with-repository --no-interaction

$CONSOLE doctrine:generate:entity --entity="CoreBundle:Enrollment" --fields="\
    user:string(120) \
    activity:string(120) \
    created_at:datetime \
    is_paid:boolean \
    is_cancelled:boolean \
    comment:text \
    " --format=annotation --with-repository --no-interaction
