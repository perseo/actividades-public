<?php

namespace Ceeps\Actividades\BackendBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ceeps\Actividades\BackendBundle\Form\EnrollmentType;
use Ceeps\Actividades\BackendBundle\Form\MessageType;
use Ceeps\Actividades\CoreBundle\Entity\Activity;
use Ceeps\Actividades\CoreBundle\Entity\Enrollment;

/**
 * Enrollment controller.
 *
 * @Route("/backend/enrollment")
 */
class EnrollmentController extends Controller
{
    /**
     * Lists all Enrollment entities.
     *
     * @Route("/{id}", name="backend_enrollment")
     * @Template()
     */
    public function indexAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        
        $activity = $em->getRepository('CoreBundle:Activity')->find($id);
        
        if (!$activity) {
            throw $this->createNotFoundException('Unable to find Activity entity.');
        }

        $entities = $em->getRepository('CoreBundle:Enrollment')->findByActivity($id, array('created_at' => 'ASC'));

        return array(
            'activity' => $activity,
            'entities' => $entities,
        );
    }

    /**
     * Finds and displays a Enrollment entity.
     *
     * @Route("/{id}/show", name="backend_enrollment_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('CoreBundle:Enrollment')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Enrollment entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        );
    }

    /**
     * Displays a form to create a new Enrollment entity.
     *
     * @Route("/{activity}/new", name="backend_enrollment_new")
     * @Template()
     */
    public function newAction($activity)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $activity = $em->getRepository('CoreBundle:Activity')->find($activity);
        
        if (!$activity) {
            throw $this->createNotFoundException('Unable to find Activity entity.');
        }
        
        $entity = new Enrollment();
        $entity->setActivity($activity);
        $form   = $this->createForm(new EnrollmentType(), $entity);
        
        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Creates a new Enrollment entity.
     *
     * @Route("/{activity}/create", name="backend_enrollment_create")
     * @Method("post")
     * @Template("BackendBundle:Enrollment:new.html.twig")
     */
    public function createAction($activity)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $activity = $em->getRepository('CoreBundle:Activity')->find($activity);
        
        if (!$activity) {
            throw $this->createNotFoundException('Unable to find Activity entity.');
        }
        
        $entity = new Enrollment();
        $entity->setActivity($activity);
        $request = $this->getRequest();
        $form    = $this->createForm(new EnrollmentType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $entity->setCreatedAt(new \DateTime('now'));
            $em->persist($entity);
            $em->flush();
            $this->get('session')->setFlash('success', 'El elemento se ha creado correctamente.');

            return $this->redirect($this->generateUrl('backend_enrollment_show', array('id' => $entity->getId())));
            
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Enrollment entity.
     *
     * @Route("/{id}/edit", name="backend_enrollment_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('CoreBundle:Enrollment')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Enrollment entity.');
        }

        $editForm = $this->createForm(new EnrollmentType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Enrollment entity.
     *
     * @Route("/{id}/update", name="backend_enrollment_update")
     * @Method("post")
     * @Template("BackendBundle:Enrollment:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('CoreBundle:Enrollment')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Enrollment entity.');
        }

        $editForm   = $this->createForm(new EnrollmentType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();
            $this->get('session')->setFlash('success', 'El elemento se ha actualizado correctamente');

            return $this->redirect($this->generateUrl('backend_enrollment_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Enrollment entity.
     *
     * @Route("/{id}/delete", name="backend_enrollment_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('CoreBundle:Enrollment')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Enrollment entity.');
            }

            $this->get('session')->setFlash('success', 'El elemento se ha eliminado correctamente');
            
            $back = $entity->getActivity()->getId();
            
            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('backend_enrollment', array('id' => $back)));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
    
    
    /**
     * Send a message to users
     * 
     * @Route("/{id}/send", name="backend_enrollment_send")
     * @Template()
     */
    public function sendAction(Activity $activity) 
    {
        $request = $this->getRequest();
        $form = $this->createForm(new MessageType());
        
        if ("POST" == $request->getMethod()) {
            
            $form->bindRequest($request);
            
            if ($form->isValid()) {
                
                $emails = array();
                
                foreach ($activity->getEnrollments() as $enrollment) {
                    $emails[] = $enrollment->getUser()->getEmail();
                }
                
                $data = $form->getData();
                
                $message = \Swift_Message::newInstance()
                        ->setSubject($data['subject'])
                        ->setFrom('noreply@consejo-eps.uco.es')
                        ->setBcc($emails)
                        ->setBody($data['body'])
                ;
                
                $this->get('mailer')->send($message);
                
                $this->get('session')->setFlash('success', '<i class="icon-envelope"></i> El mensaje se ha enviado con éxito');
                
                return $this->redirect($this->generateUrl('backend_enrollment', array('id' => $activity->getId())));
            }
            
        }
        
        
        return array (
            'form' => $form->createView(),
            'entity' => $activity,
        );
    }
}
