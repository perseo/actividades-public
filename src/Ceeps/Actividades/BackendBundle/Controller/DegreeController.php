<?php

namespace Ceeps\Actividades\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ceeps\Actividades\CoreBundle\Entity\Degree;
use Ceeps\Actividades\BackendBundle\Form\DegreeType;

/**
 * Degree controller.
 *
 * @Route("/backend/degree")
 */
class DegreeController extends Controller
{
    /**
     * Lists all Degree entities.
     *
     * @Route("/", name="backend_degree")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('CoreBundle:Degree')->findBy(array(), array('name'=>'ASC'));

        return array('entities' => $entities);
    }

    /**
     * Finds and displays a Degree entity.
     *
     * @Route("/{id}/show", name="backend_degree_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('CoreBundle:Degree')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Degree entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        );
    }

    /**
     * Displays a form to create a new Degree entity.
     *
     * @Route("/new", name="backend_degree_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Degree();
        $form   = $this->createForm(new DegreeType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Creates a new Degree entity.
     *
     * @Route("/create", name="backend_degree_create")
     * @Method("post")
     * @Template("BackendBundle:Degree:new.html.twig")
     */
    public function createAction()
    {
        $entity  = new Degree();
        $request = $this->getRequest();
        $form    = $this->createForm(new DegreeType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->setFlash('success', 'El elemento se ha creado correctamente.');

            return $this->redirect($this->generateUrl('backend_degree_show', array('id' => $entity->getId())));
            
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Degree entity.
     *
     * @Route("/{id}/edit", name="backend_degree_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('CoreBundle:Degree')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Degree entity.');
        }

        $editForm = $this->createForm(new DegreeType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Degree entity.
     *
     * @Route("/{id}/update", name="backend_degree_update")
     * @Method("post")
     * @Template("BackendBundle:Degree:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('CoreBundle:Degree')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Degree entity.');
        }

        $editForm   = $this->createForm(new DegreeType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->setFlash('success', 'El elemento se ha actualizado correctamente');

            return $this->redirect($this->generateUrl('backend_degree_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Degree entity.
     *
     * @Route("/{id}/delete", name="backend_degree_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('CoreBundle:Degree')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Degree entity.');
            }

            $em->remove($entity);
            $em->flush();

            $this->get('session')->setFlash('success', 'El elemento se ha eliminado correctamente');
        }

        return $this->redirect($this->generateUrl('backend_degree'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
