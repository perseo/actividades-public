<?php

namespace Ceeps\Actividades\BackendBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

/**
 * Description of MenuBuilder
 *
 * @author sergio
 */
class Builder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root', array('currentClass' => 'active'));
        $menu->setCurrentUri($this->container->get('request')->getRequestUri());
        $menu->setChildrenAttribute('class', 'nav');
        
        $menu->addChild('Inicio', array('route' => 'backend'));
        $menu->addChild('Usuarios', array('route' => 'backend_user'));
        $menu->addChild('Grados', array('route' => 'backend_degree'));
        $menu->addChild('Actividades', array('route' => 'backend_activity'));
        $menu->addChild('Volver al frontend', array( 'route' => 'homepage'));

        return $menu;
    }
}
