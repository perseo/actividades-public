<?php

namespace Ceeps\Actividades\BackendBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class EnrollmentType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('activity', null, array(
                'label' => 'Actividad',
                'read_only' => true,
            ))
            ->add('user', 'genemu_jquerychosen', array(
                'label' => 'Usuario',
                'read_only' => true,
                'widget' => 'entity',
                'class' => 'CoreBundle:User',
                'empty_value' => 'Elija un usuario',
                'query_builder' => function(EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('u')
                    ->orderBy('u.name', 'ASC')
                    ->where('u NOT IN (SELECT u2 FROM CoreBundle:Enrollment e JOIN e.user u2 WHERE e.activity = :activity)')
                    ->addOrderBy('u.lastname', 'ASC')
                    ->setParameter('activity', $options['data']->getActivity() )
                    ;
                }, 
            ))
            ->add('created_at', null, array(
                'label' => 'Fecha de creación',
                'widget' => 'single_text',
                'read_only' => true,
            ))
            ->add('is_paid', null, array(
                'label' => 'Marcada como pagada',
                'required' => false,
            ))
            ->add('is_cancelled', null, array(
                'label' => 'Marcada como cancelada',
                'required' => false,
            ))
            ->add('comment', 'textarea', array(
                'label' => 'Comentario del usuario',
                'required' => false,
            ))
        ;
        
        if (null === $options['data']->getId()) {
            $builder->get('user')->setReadOnly(false);
        }
        
    }

    public function getName()
    {
        return 'ceeps_actividades_corebundle_enrollmenttype';
    }
}
