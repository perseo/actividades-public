<?php

namespace Ceeps\Actividades\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class DegreeType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'label' => 'Nombre',
            ))
        ;
    }

    public function getName()
    {
        return 'ceeps_common_userbundle_degreetype';
    }
}
