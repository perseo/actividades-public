<?php

namespace Ceeps\Actividades\BackendBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class UserType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('username', 'text', array(
                'label' => 'Nombre de usuario',
                'read_only' => true,
            ))
            ->add('name', 'text', array(
                'label' => 'Nombre',
            ))
            ->add('lastname', 'text', array(
                'label' => 'Apellidos',
            ))
            ->add('phonenumber', 'text', array(
                'label' => 'Teléfono',
            ))
            ->add('email', 'text', array(
                'label' => 'Correo electrónico',
            ))
            ->add('year', 'text', array(
                'label' => 'Curso',
            ))
            ->add('degree',null, array(
                'label' => 'Titulación',
                'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('d')
                        ->orderBy('d.name', 'ASC');
                },  
            ))
            ->add('userRoles', null, array(
                'label' => 'Permisos',
            ))
        ;
    }

    public function getName()
    {
        return 'ceeps_actividades_corebundle_usertype';
    }
}
