<?php

namespace Ceeps\Actividades\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class ActivityType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'label' => 'Nombre',
            ))
            ->add('description', 'textarea', array(
                'label' => 'Descripción'
            ))
            ->add('url', 'url', array(
                'label' => 'URL referencia',
                'required' => false,
            ))
            ->add('starts_at', 'genemu_jquerydate', array(
                'label' => 'Fecha de inicio de la actividad',
                'widget' => 'single_text',
                'required' => false,
            ))
            ->add('ends_at', 'genemu_jquerydate', array(
                'label' => 'Fecha de fin de la actividad',
                'widget' => 'single_text',
                'required' => false,
            ))
            ->add('registration_starts_at', 'genemu_jquerydate', array(
                'label' => 'Fecha de inicio de la inscripción',
                'widget' => 'single_text',
                'required' => false,
            ))
            ->add('registration_ends_at', 'genemu_jquerydate', array(
                'label' => 'Fecha de inicio de la inscripción',
                'widget' => 'single_text',
                'required' => false,
            ))
            ->add('seats', 'integer', array(
                'label' => 'Plazas',
            ))
            ->add('price', 'money', array(
                'label' => 'Precio',
            ))
            ->add('deposit', 'money', array(
                'label' => 'Depósito',
            ))
        ;
    }

    public function getName()
    {
        return 'ceeps_actividades_corebundle_activitytype';
    }
}
