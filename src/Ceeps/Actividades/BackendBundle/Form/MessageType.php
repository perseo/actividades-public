<?php

namespace Ceeps\Actividades\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\MaxLength;
use Symfony\Component\Validator\Constraints\MinLength;

class MessageType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('subject', 'text', array (
                'label' => 'Asunto',
            ))
            ->add('body', 'textarea', array(
                'label' => 'Mensaje',
                'required' => false,
                "attr" => array("rows" => "10"),
            ))
        ;
    }

    public function getDefaultOptions(array $options)
    {
        $collectionConstraint = new Collection(array(
           'subject' => array(new MinLength(5), new MaxLength(255)),
           'body' => new MinLength(5),
        ));
        
        return array('validation_constraint' => $collectionConstraint);
    }
    
    public function getName()
    {
        return 'ceeps_actividades_backendbundle_messagetype';
    }
}
