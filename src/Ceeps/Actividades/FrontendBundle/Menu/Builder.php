<?php

namespace Ceeps\Actividades\FrontendBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory)
    {
        $menu = $factory->createItem('root', array('currentClass' => 'active'));
        $menu->setCurrentUri($this->container->get('request')->getRequestUri());
        $menu->setChildrenAttribute('class', 'nav nav-list');

        $menu->addChild('Actividades');
        $menu['Actividades']->setAttribute('class', 'nav-header');
        $menu->addChild('Próximas actividades', array('route' => 'activity'));
        $menu->addChild('Actividades abiertas', array('route' => 'activity_open'));
        $menu->addChild('Actividades previstas (sin fecha)', array('route' => 'activity_comming'));

        return $menu;
    }
    
    public function loginMenu(FactoryInterface $factory)
    {
        $menu = $factory->createItem('login', array('currentClass' => 'active'));
        $menu->setCurrentUri($this->container->get('request')->getRequestUri());
        $menu->setChildrenAttribute('class', 'nav pull-right');
        $menu->addChild('Iniciar sesión', array('route' => 'my_login'));
        $menu->addChild('Crear nueva cuenta', array('route' => 'my_register'));

        return $menu;
    }
}