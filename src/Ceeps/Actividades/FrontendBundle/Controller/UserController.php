<?php

namespace Ceeps\Actividades\FrontendBundle\Controller;

use Ceeps\Actividades\CoreBundle\Entity\User;
use Ceeps\Actividades\FrontendBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * @Route("/my") 
 */
class UserController extends Controller
{
    
    /**
     * Register action
     * 
     * @Route("/register", name="my_register")
     * @Template() 
     */
    public function registerAction()
    {
        $request = $this->getRequest();
        
        $user = new User();
        $form = $this->createForm(new UserType(), $user);
        
        if ("POST" == $request->getMethod()) {
            
            $form->bindRequest($request);
            
            if (true === $form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                
                $role = $em->getRepository('CoreBundle:Role')->findOneByName('ROLE_USER');
                
                $user->setSalt(md5(rand()));
                $user->addRole($role);
                
                $encoder = $this->get('security.encoder_factory')->getEncoder($user);
                $password = $encoder->encodePassword(
                        $user->getPassword(),
                        $user->getSalt()
                );
                $user->setPassword($password);
                
                $em->persist($user);
                $em->flush();
                
                $this->get('session')->setFlash('info','Enhorabuena, te has registrado en la aplicación');
                
                $token = new UsernamePasswordToken($user, $user->getPassword(), 'users', $user->getRoles());
                $this->container->get('security.context')->setToken($token);
                
                return $this->redirect($this->generateUrl('my_profile_edit'));
            }
        }
        
        return array (
            'form' => $form->createView(),
        );
    }
    
    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/profile", name="my_profile_edit")
     * @Template()
     */
    public function editAction()
    {
        $entity = $this->get('security.context')->getToken()->getUser();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm = $this->createForm(new UserType(), $entity);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        );
    }
    
    
    /**
     * Edits an existing User entity.
     *
     * @Route("/profile/update", name="my_profile_update")
     * @Method("post")
     * @Template("FrontendBundle:User:edit.html.twig")
     */
    public function updateAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $this->get('security.context')->getToken()->getUser();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm   = $this->createForm(new UserType(), $entity);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $encoder = $this->get('security.encoder_factory')->getEncoder($entity);
            $password = $encoder->encodePassword(
                    $entity->getPassword(),
                    $entity->getSalt()
            );
            $entity->setPassword($password);

            $em->persist($entity);
            $em->flush();
            
            $this->get('session')->setFlash('success', 'Tu perfil se ha actualizado correctamente.');

            return $this->redirect($this->generateUrl('my_profile_edit'));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        );
    }
    
    
    /**
     * Login action
     * 
     * @Route("/login", name="my_login")
     * @Template() 
     */
    public function loginAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();

        if (null !== $user && true === $this->get('security.context')->isGranted('ROLE_USER')) {
            return $this->redirect($this->generateUrl('my_enroll'));
        }
        
        $request = $this->getRequest();
        $session = $request->getSession();
        
        
        $error = $request->attributes->get(
            SecurityContext::AUTHENTICATION_ERROR,
            $session->get(SecurityContext::AUTHENTICATION_ERROR)
        );
        
        return array(
            'last_username' => $session->get(SecurityContext::LAST_USERNAME),
            'error'         => $error,
        );
    }
    
    /**
     * Logout action
     * 
     * @Route("/logout", name="my_logout")
     * @Template() 
     */
    public function logoutAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();
        
        if (null == $user || 
            !$this->get('security.context')->isGranted('ROLE_USER')) {
            $this->get('session')->setFlash('error',
                'Para darte de baja primero tienes que conectarte.'
            );
            die();
            return $this->redirect($this->generateUrl('my_login'));
        }
        
        $this->get('request')->getSession()->invalidate();
        $this->get('security.context')->setToken(null);
        
        return $this->redirect($this->generateUrl('homepage'));        
    }
    
    /**
     * Login Check action
     * 
     * @Route("/login_check", name="my_login_check")
     * @Template() 
     */
    public function logcheckAction()
    {
    }
    
       /**
     *
     * @Route("/enroll", name="my_enroll")
     * @Template()
     */
    public function enrollAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        
        $entity = $this->get('security.context')->getToken()->getUser();
        
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        return array(
            'entities'    => $entity->getEnrollments()
        );
    }
    
}
