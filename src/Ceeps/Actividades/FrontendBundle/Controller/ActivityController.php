<?php

namespace Ceeps\Actividades\FrontendBundle\Controller;

use Ceeps\Actividades\CoreBundle\Entity\Activity;
use Ceeps\Actividades\CoreBundle\Entity\Enrollment;
use Ceeps\Actividades\FrontendBundle\Form\EnrollmentType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Activity Controller
 * 
 * @Route("/activity") 
 */
class ActivityController extends Controller
{
    /**
     * Lists all Activity entities.
     *
     * @Route("/", name="activity")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('CoreBundle:Activity')->findNextActivities();

        return array(
            'entities' => $entities, 
            'title' => 'Próximas actividades'
        );
    }

    /**
     * Finds and displays a Activity entity.
     *
     * @Route("/{slug}/show", name="activity_show")
     * @Template()
     */
    public function showAction(Activity $entity)
    {
        return array(
            'entity' => $entity,
        );
    }
    
    /**
     * List all open activities
     * 
     * @Route("/open", name="activity_open")
     * @Template("FrontendBundle:Activity:index.html.twig")
     */
    public function openAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        
        $entities = $em->getRepository('CoreBundle:Activity')->findOpenActivities();
        
        return array(
            'entities' => $entities, 
            'title' => 'Actividades abiertas'
        );
    }

    /**
     * List all open activities
     * 
     * @Route("/comming", name="activity_comming")
     * @Template("FrontendBundle:Activity:index.html.twig")
     */    
    public function commingAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        
        $entities = $em->getRepository('CoreBundle:Activity')->findCommingActivities();
        
        return array(
            'entities' => $entities, 
            'title' => 'Actividades previstas (sin fecha)'
        );        
    }
    
    /**
     * Enroll user to an activity
     *
     * @Route("/{slug}/enroll", name="activity_enroll")
     * @Template()
     */
    public function enrollAction(Activity $activity)
    {        
        $em = $this->getDoctrine()->getEntityManager();

        if (false === $activity->isOpen()) {
            $this->get('session')->setFlash('error', 'El plazo está cerrado. No es posible inscribirse a "'.$activity.'".');
        
            return $this->redirect($this->generateUrl('activity_open'));
        }
        
        $user = $this->get('security.context')->getToken()->getUser();
        
        if ($user->isEnrolled($activity)) {
            $this->get('session')->setFlash('error', 'Ya estabas suscrito a "'.$activity.'".');
            
            return $this->redirect($this->generateUrl('my_enroll'));
        }
        
        if ($activity->isFull()) {
            $this->get('session')->setFlash('error', 'No hay plazas libres en "'.$activity.'".');
        
            return $this->redirect($this->generateUrl('activity_open'));
        }
        
        $entity = new Enrollment();
        $entity->setUser($user);
        $entity->setActivity($activity);
        $form = $this->createForm(new EnrollmentType(), $entity);

        if ($this->getRequest()->getMethod() == "POST")
        {
            $form->bindRequest($this->getRequest());
            
            if ($form->isValid()) {
                $entity->setCreatedAt(new \DateTime('now'));
                $em->persist($entity);
                $em->flush();
                
                $this->get('session')->setFlash('success', 'Enhorabuena te has suscrito con éxito a "'.$activity.'".');
                
                return $this->redirect($this->generateUrl('my_enroll'));
            }
        }            
        
        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Enroll user to an activity
     *
     * @Route("/{slug}/unenroll", name="activity_unenroll")
     * @Template()
     */    
    public function unenrollAction(Activity $activity) 
    {
        $em = $this->getDoctrine()->getEntityManager();
        $user = $this->get('security.context')->getToken()->getUser();

        $entity = $em->getRepository('CoreBundle:Enrollment')->findByActivityAndUser($activity, $user);
        
        if (!$entity) {
            $this->get('session')->setFlash('error', 'No estabas inscrito a "'.$activity.'".');
            
            return $this->redirect($this->generateUrl('my_enroll'));
        }
        
        $em->remove($entity);
        $em->flush();
        
        $this->get('session')->setFlash('success', 'Ya no estás inscrito a "'.$activity.'".');

        return $this->redirect($this->generateUrl('my_enroll'));
    }
    
}
