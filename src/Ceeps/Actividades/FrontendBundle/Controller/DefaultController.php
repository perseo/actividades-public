<?php

namespace Ceeps\Actividades\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        
        $activities = $em->getRepository("CoreBundle:Activity")->findNextActivities(4);
        
        return array(
            "activities" => $activities,
        );
    }
    
    /**
     * @Route("/register", name="register")
     * Template()
     */
    public function registerAction()
    {
        return array();
    }
}
