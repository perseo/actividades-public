<?php

namespace Ceeps\Actividades\FrontendBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class UserType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('username', null, array (
                'label' => 'Nombre de usuario',
            ))
            ->add('password', 'repeated', array(
                'type' => 'password',
                'invalid_message' => 'Las dos contraseñas deben coincidir',
                'options' => array('label' => 'Contraseña'),
            ))
            ->add('name', null, array (
                'label' => 'Nombre',
            ))
            ->add('lastname', null, array(
                'label' => 'Apellidos',
            ))
            ->add('phonenumber', null, array(
                'label' => 'Teléfono',
                'required' => false,
            ))
            ->add('email', 'email', array(
                'label' => 'Correo electrónico',
            ))
            ->add('degree', null, array(
                'label' => 'Titulación',
                'empty_value' => 'Elige una titulación',
                'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('d')
                        ->orderBy('d.name', 'ASC');
                }, 
            ))
            ->add('year', null, array(
                'label' => 'Curso',
                'required' => false,
            ))
        ;
    }

    public function getName()
    {
        return 'ceeps_actividades_frontendbundle_usertype';
    }
}
