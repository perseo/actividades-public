<?php

namespace Ceeps\Actividades\FrontendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class EnrollmentType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('comment', 'textarea', array(
                'label' => 'Comentario',
                'required' => false,
            ))
        ;
    }

    public function getName()
    {
        return 'ceeps_actividades_frontendbundle_enrollmenttype';
    }
}
