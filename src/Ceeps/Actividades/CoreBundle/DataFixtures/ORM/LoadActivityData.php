<?php

namespace Ceeps\Actividades\CoreBundle\DataFixtures\ORM;

use Ceeps\Actividades\CoreBundle\Entity\Activity;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadActivityData implements FixtureInterface
{
    public function load(ObjectManager $manager) 
    {
        $description="Nullam id iaculis est. Aenean tempor lobortis quam, at 
rhoncus nisl iaculis vel. Proin ac odio lorem, sed ullamcorper arcu. 
Curabitur sodales metus a arcu elementum ultrices. Fusce egestas 
dignissim semper. Vivamus orci lectus, sagittis sed laoreet ac, 
iaculis rutrum neque. Aenean et enim quis massa fringilla molestie 
vel at odio. Nullam et enim eu massa hendrerit pharetra quis in diam. 
Pellentesque habitant morbi tristique senectus et netus et malesuada 
fames ac turpis egestas. Quisque vitae ultrices ante. Proin nibh 
sapien, dignissim ut varius id, lacinia mattis arcu. Vivamus vitae 
felis eu lacus ultricies semper. Donec ligula arcu, aliquam ac aliquet 
vel, pellentesque vitae erat. Pellentesque risus ipsum, viverra 
vitae dignissim vitae, posuere et velit.";
        
        $activities = array(
          array(
              "name" => "Taller de introducción a Symfony 2",
              "url" => "http://consejo-eps.uco.es/curso-symfony2",
              "startsAt" => new \DateTime('2012/03/1'),
              "endsAt" => new \DateTime('2012/03/1'),
              "registrationStartsAt" => new \DateTime('2012/02/16'),
              "registrationEndsAt" => new \DateTime('2012/02/29'),
              "seats" => 30,
              "price" => 0,
              "deposit" => 0,
          ),
          array(
              "name" => "Taller de introducción a Drupal",
              "url" => "http://consejo-eps.uco.es/curso-drupal",
              "startsAt" => new \DateTime('2012/04/2'),
              "endsAt" => new \DateTime('2012/04/5'),
              "registrationStartsAt" => new \DateTime('2012/02/16'),
              "registrationEndsAt" => new \DateTime('2012/04/22'),
              "seats" => 30,
              "price" => 10,
              "deposit" => 0,
          ),
          array(
              "name" => "Taller de introducción a Wordpress",
              "url" => "http://consejo-eps.uco.es/curso-wordpress",
              "startsAt" => new \DateTime('2012/05/3'),
              "endsAt" => new \DateTime('2012/05/3'),
              "registrationStartsAt" => new \DateTime('2012/02/10'),
              "registrationEndsAt" => new \DateTime('2012/04/2'),
              "seats" => 30,
              "price" => 10,
              "deposit" => 5,
          ),
          array(
              "name" => "Taller de introducción a Joomla",
              "url" => "http://consejo-eps.uco.es/curso-joomla",
              "startsAt" => new \DateTime('2012/06/23'),
              "endsAt" => new \DateTime('2012/06/23'),
              "registrationStartsAt" => new \DateTime('2012/03/16'),
              "registrationEndsAt" => new \DateTime('2012/04/22'),
              "seats" => 30,
              "price" => 0,
              "deposit" => 5,
          ),
          array(
              "name" => "Taller de papiroflexia",
              "url" => "http://consejo-eps.uco.es/curso-papiroflexia",
              "startsAt" => null,
              "endsAt" => null,
              "registrationStartsAt" => null,
              "registrationEndsAt" => null,
              "seats" => 0,
              "price" => 0,
              "deposit" => 5,
          ),        
        );
        
        foreach ($activities as $activity) {
            
            extract($activity);
            
            $object = new Activity();
            $object->setName($name);
            $object->setUrl($url);
            $object->setDescription($description);
            $object->setStartsAt($startsAt);
            $object->setEndsAt($endsAt);
            $object->setRegistrationStartsAt($registrationStartsAt);
            $object->setRegistrationEndsAt($registrationEndsAt);
            $object->setSeats($seats);
            $object->setPrice($price);
            $object->setDeposit($deposit);
            
            $manager->persist($object);
        }
        
        $manager->flush();
    }
}