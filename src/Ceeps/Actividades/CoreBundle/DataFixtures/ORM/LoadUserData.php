<?php

namespace Ceeps\Actividades\CoreBundle\DataFixtures\ORM;

use Ceeps\Actividades\CoreBundle\Entity\Degree;
use Ceeps\Actividades\CoreBundle\Entity\User;
use Ceeps\Actividades\CoreBundle\Entity\Role;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

class LoadUserData extends AbstractFixture implements FixtureInterface
{
    public function load(ObjectManager $manager) 
    {
        $roles = array ('ROLE_USER', 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN');
        
        foreach ($roles as $role)
        {
            $entity = new Role();
            $entity->setName($role);
            $manager->persist($entity);
            $this->setReference($role, $entity);
        }

        $degrees = array(
            "Ingeniería Técnica en Informática de Sistemas",
            "Ingeniería Técnica en Informática de Gestión",
            "Ingeniería en Informática (2º ciclo)",
            "Grado en Ingeniería Informática",
            "Ingeniería Técnica Industrial (Especialidad Electrónica)",
            "Ingeniería Técnica Industrial (Especialidad Electricidad)",
            "Ingeniería Técnica Industrial (Especialidad Mecánica)",
            "Ingeniería en Automática y Electrónica Industrial (2º ciclo)",
            "Grado en Ingeniería Electrónica Industrial",
            "Grado en Ingeniería Eléctrica",
            "Grado en Ingeniería Mecánica",
            "Egresado",
        );
        
        foreach ($degrees as $degree)
        {
            $entity = new Degree();
            $entity->setName($degree);
            $manager->persist($entity);
        }

        $names = array ("Sergio", "Luis", "Francisco", "Juan", "Pablo", "Carmen", "María", "Cristina", "Rocío", "Eva");
        $lastnames = array ("Gómez", "de Benito", "Franco", "Motos", "Carbonell", "de Mairena", "del Monte", "Rosenvinge", "Jurado", "Longoria");
        $encoder = new MessageDigestPasswordEncoder('sha512', true, 10);
        
        $users = array(
            array (
                'username'  => 'admin',
                'role'      => $this->getReference('ROLE_ADMIN'),
            ),
        );
        
        for ($i=0; $i<50; $i++)
        {
            $user = array (
                'username'  => sprintf ("user%02d", $i),
                'role'      => $this->getReference('ROLE_USER'),
            );
            
            $users[] = $user;
        }
        
        foreach ($users as $user)
        {
            extract($user);
                        
            $entity = new User();
            $entity->setUsername($username);
            $entity->setName($names[array_rand($names)]);
            $entity->setLastname($lastnames[array_rand($lastnames)]." ".$lastnames[array_rand($lastnames)]);
            $entity->setEmail($username."@uco.es");
            $entity->setSalt(md5(rand()));
            $entity->setPassword($encoder->encodePassword($username, $entity->getSalt()));
            $entity->addRole($role);
            $manager->persist($entity);
        }

        $manager->flush();
    }
}