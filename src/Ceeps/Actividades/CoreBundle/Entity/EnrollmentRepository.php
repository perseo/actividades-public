<?php

namespace Ceeps\Actividades\CoreBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * EnrollmentRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class EnrollmentRepository extends EntityRepository
{
    public function findByActivityAndUser(Activity $activity, User $user)
    {
        $em = $this->getEntityManager();
        
        $dql = $this->createQueryBuilder('e')
                ->where('e.user = :user')
                ->andWhere('e.activity = :activity')
        ;
        
        return $em->createQuery($dql)
                ->setParameter('activity', $activity)
                ->setParameter('user', $user)
                ->getOneOrNullResult();
        ;
                
    }
}