<?php

namespace Ceeps\Actividades\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Ceeps\Actividades\CoreBundle\Entity\User
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ceeps\Actividades\CoreBundle\Entity\UserRepository")
 * @UniqueEntity(fields={"username"},message="El nombre de usuario ya se encuentra en uso")
 * @UniqueEntity(fields={"email"},message="Ya existe una cuenta con esa dirección")
 */
class User implements UserInterface
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $username
     *
     * @ORM\Column(name="username", type="string", length=120)
     * @Assert\MinLength(3)
     * @Assert\MaxLength(120)
     */
    private $username;

    /**
     * @var string $password
     *
     * @ORM\Column(name="password", type="string", length=120)
     * @Assert\MinLength(7)
     * @Assert\MaxLength(120)
     */
    private $password;

    /**
     * @var string $salt
     *
     * @ORM\Column(name="salt", type="string", length=120)
     */
    private $salt;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=120)
     * @Assert\MaxLength(120)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string $lastname
     *
     * @ORM\Column(name="lastname", type="string", length=120)
     * @Assert\MaxLength(120)
     * @Assert\NotBlank()
     */
    private $lastname;

    /**
     * @var string $phonenumber
     *
     * @ORM\Column(name="phonenumber", type="string", length=50, nullable=true)
     */
    private $phonenumber;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=120, unique=true)
     * @Assert\Email()
     * @Assert\MaxLength(120)
     * @Assert\NotBlank()
     */
    private $email;

    /**
     * @var string $degree
     *
     * @ORM\ManyToOne(targetEntity="Degree")
     */
    private $degree;

    /**
     * @var smallint $year
     *
     * @ORM\Column(name="year", type="smallint", nullable=true)
     * @Assert\Max(5)
     * @Assert\Min(1)
     */
    private $year;

    /**
     * @var string $userRoles
     *
     * @ORM\ManyToMany(targetEntity="Role")
     * @ORM\JoinTable(name="UserRole",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     * )
     */
    private $userRoles;

    /**
     * @var string $enrollments
     *
     * @ORM\OneToMany(targetEntity="Enrollment", mappedBy="user")
     */
    private $enrollments;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;

    /**
     * @var datetime $lastlogon
     *
     * @ORM\Column(name="lastlogon", type="datetime", nullable=true)
     */
    private $lastlogon;

    /**
     * User constructor 
     */
    public function __construct()
    {
        $this->userRoles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->enrollments = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param string $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * Get salt
     *
     * @return string 
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set phonenumber
     *
     * @param string $phonenumber
     */
    public function setPhonenumber($phonenumber)
    {
        $this->phonenumber = $phonenumber;
    }

    /**
     * Get phonenumber
     *
     * @return string 
     */
    public function getPhonenumber()
    {
        return $this->phonenumber;
    }

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get email hash for gravatar
     * 
     * @return string
     */
    public function getEmailHash()
    {
        return md5(strtolower(trim($this->getEmail())));
    }


    /**
     * Set year
     *
     * @param smallint $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * Get year
     *
     * @return smallint 
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * Get created_at
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set lastlogon
     *
     * @param datetime $lastlogon
     */
    public function setLastlogon($lastlogon)
    {
        $this->lastlogon = $lastlogon;
    }

    /**
     * Get lastlogon
     *
     * @return datetime 
     */
    public function getLastlogon()
    {
        return $this->lastlogon;
    }

    /**
     * {@inheritDoc}
     */
    public function equals(UserInterface $user)
    {
        return md5($this->getUsername()) == md5($user->getUsername());
    }

    /**
     * {@inheritDoc}
     */
    public function eraseCredentials()
    {
        
    }

    /**
     * Set degree
     *
     * @param Ceeps\Actividades\CoreBundle\Entity\Degree $degree
     */
    public function setDegree(\Ceeps\Actividades\CoreBundle\Entity\Degree $degree)
    {
        $this->degree = $degree;
    }

    /**
     * Get degree
     *
     * @return Ceeps\Actividades\CoreBundle\Entity\Degree 
     */
    public function getDegree()
    {
        return $this->degree;
    }

    /**
     * Add userRoles
     *
     * @param Ceeps\Actividades\CoreBundle\Entity\Role $userRoles
     */
    public function addRole(\Ceeps\Actividades\CoreBundle\Entity\Role $userRoles)
    {
        $this->userRoles[] = $userRoles;
    }

    /**
     * {@inheritDoc}
     */
    public function getRoles()
    {
        return array_map( 
                function($obj) { return $obj->getName(); }, 
                $this->getUserRoles()->toArray()
        );
    }

    /**
     * Get userRoles
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getUserRoles()
    {
        return $this->userRoles;
    }

    /**
     * Add enrollments
     *
     * @param Ceeps\Actividades\CoreBundle\Entity\Enrollment $enrollments
     */
    public function addEnrollment(\Ceeps\Actividades\CoreBundle\Entity\Enrollment $enrollments)
    {
        $this->enrollments[] = $enrollments;
    }

    /**
     * Get enrollments
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getEnrollments()
    {
        return $this->enrollments;
    }
    
    /**
     * Check if user is enrolled in one activity
     * 
     * @param Activity $activity
     * @return boolean 
     */
    public function isEnrolled($activity)
    {
        foreach ($this->getEnrollments() as $enroll)
        {
            if ($enroll->getActivity() == $activity) return true;
        }
        
        return false;
    }
    
    /**
     * Return string representation
     * 
     * @return string
     */
    public function __toString()
    {
        return sprintf("%s %s - %s", $this->getName(),$this->getLastname(),$this->getUsername());
    }
    
    /**
     * Serialize data
     * 
     * @return array
     */
    public function __sleep()
    {
        return array('id', 'username');
    }
}