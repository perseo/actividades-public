<?php

namespace Ceeps\Actividades\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContext;

/**
 * Ceeps\Actividades\CoreBundle\Entity\Activity
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ceeps\Actividades\CoreBundle\Entity\ActivityRepository")
 * @Assert\Callback(methods={"isDateValid"})
 */
class Activity
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=120)
     * @Assert\MaxLength(120)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string $slug
     *
     * @ORM\Column(name="slug", type="string", length=120, unique=true)
     * @Gedmo\Slug(fields={"name"})
     */
    private $slug;

    /**
     * @var text $description
     *
     * @ORM\Column(name="description", type="text")
     * @Assert\NotBlank()
     */
    private $description;

    /**
     * @var string $url
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     * @Assert\MaxLength(255)
     */
    private $url;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Assert\DateTime()
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     * @Assert\DateTime()
     */
    private $updated_at;

    /**
     * @var datetime $starts_at
     *
     * @ORM\Column(name="starts_at", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $starts_at;

    /**
     * @var datetime $ends_at
     *
     * @ORM\Column(name="ends_at", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $ends_at;

    /**
     * @var datetime $registration_starts_at
     *
     * @ORM\Column(name="registration_starts_at", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $registration_starts_at;

    /**
     * @var datetime $registration_ends_at
     *
     * @ORM\Column(name="registration_ends_at", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $registration_ends_at;

    /**
     * @var float $price
     *
     * @ORM\Column(name="price", type="float")
     * @Assert\Type("float")
     * @Assert\Min(0)
     */
    private $price;

    /**
     * @var float $deposit
     *
     * @ORM\Column(name="deposit", type="float")
     * @Assert\Type("float")
     * @Assert\Min(0)
     */
    private $deposit;

    /**
     * @var smallint $seats
     *
     * @ORM\Column(name="seats", type="smallint")
     * @Assert\Min(0)
     */
    private $seats;

    /**
     * @var string $enrollments
     *
     * @ORM\OneToMany(targetEntity="Enrollment", mappedBy="activity")
     */
    private $enrollments;


    /**
     *  Class constructor 
     */
    public function __construct()
    {
        $this->enrollments = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set description
     *
     * @param text $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return text 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set url
     *
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * Get created_at
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    /**
     * Get updated_at
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set starts_at
     *
     * @param datetime $startsAt
     */
    public function setStartsAt($startsAt)
    {
        $this->starts_at = $startsAt;
    }

    /**
     * Get starts_at
     *
     * @return datetime 
     */
    public function getStartsAt()
    {
        return $this->starts_at;
    }

    /**
     * Set ends_at
     *
     * @param datetime $endsAt
     */
    public function setEndsAt($endsAt)
    {
        $this->ends_at = $endsAt;
    }

    /**
     * Get ends_at
     *
     * @return datetime 
     */
    public function getEndsAt()
    {
        return $this->ends_at;
    }

    /**
     * Set registration_starts_at
     *
     * @param datetime $registrationStartsAt
     */
    public function setRegistrationStartsAt($registrationStartsAt)
    {
        $this->registration_starts_at = $registrationStartsAt;
    }

    /**
     * Get registration_starts_at
     *
     * @return datetime 
     */
    public function getRegistrationStartsAt()
    {
        return $this->registration_starts_at;
    }

    /**
     * Set registration_ends_at
     *
     * @param datetime $registrationEndsAt
     */
    public function setRegistrationEndsAt($registrationEndsAt)
    {
        $this->registration_ends_at = $registrationEndsAt;
    }

    /**
     * Get registration_ends_at
     *
     * @return datetime 
     */
    public function getRegistrationEndsAt()
    {
        return $this->registration_ends_at;
    }

    /**
     * Set price
     *
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set deposit
     *
     * @param float $deposit
     */
    public function setDeposit($deposit)
    {
        $this->deposit = $deposit;
    }

    /**
     * Get deposit
     *
     * @return float 
     */
    public function getDeposit()
    {
        return $this->deposit;
    }

    /**
     * Set seats
     *
     * @param smallint $seats
     */
    public function setSeats($seats)
    {
        $this->seats = $seats;
    }

    /**
     * Get seats
     *
     * @return smallint 
     */
    public function getSeats()
    {
        return $this->seats;
    }
    
    /**
     * Get seats left
     * 
     * @return smallint
     */
    public function getSeatsLeft()
    {
        return $this->getSeats() - count($this->getEnrollments());
    }

    /**
     * Add enrollments
     *
     * @param Ceeps\Actividades\CoreBundle\Entity\Enrollment $enrollments
     */
    public function addEnrollment(Enrollment $enrollments)
    {
        $this->enrollments[] = $enrollments;
    }

    /**
     * Get enrollments
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getEnrollments()
    {
        return $this->enrollments;
    }
    
    /**
     * Check if the activity is free (like a free beer)
     * 
     * @return boolean 
     */
    public function isFree()
    {
        return $this->getPrice() + $this->getDeposit() > 0 ? false : true;
    }
    
    /**
     * Check if the registration is open
     * 
     * @return boolean 
     */
    public function isOpen()
    {
        $now = new \DateTime('now');
        
        if ($this->getRegistrationStartsAt() === null || $this->getRegistrationEndsAt() === null)
            return false;
        
        if ($this->getRegistrationStartsAt() > $now || $this->getRegistrationEndsAt() < $now) 
            return false;
        
        return true;
    }
    
    /**
     * Check if the activity is full
     * 
     * @return boolean 
     */
    public function isFull()
    {
        if (0 == $this->getSeats()) return false;
        
        if ($this->getSeatsLeft() > 0) return false;
        
        return true;
    }
    
    /**
     * Return string representation
     * 
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }
    
    public function isDateValid(ExecutionContext $context)
    {
        $propertyPath = $context->getPropertyPath();
        
        if ($this->getStartsAt() > $this->getEndsAt()) {
            $field = $propertyPath . '.starts_at';
            $context->setPropertyPath($field);
            $context->addViolation('La fecha de inicio debe ser anterior a la final', array(), null);
        }
        
        if ($this->getStartsAt() === null && $this->getEndsAt() !== null) {
            $field = $propertyPath . '.starts_at';
            $context->setPropertyPath($field);
            $context->addViolation('Se debe especificar a la vez la fecha de inicio y fin', array(), null);            
        }

        if ($this->getRegistrationStartsAt() > $this->getRegistrationEndsAt()) {
            $field = $propertyPath . '.registration_starts_at';
            $context->setPropertyPath($field);
            $context->addViolation('La fecha de inicio debe ser anterior a la final', array(), null);
        }
        
        if ($this->getRegistrationStartsAt() === null && $this->getRegistrationEndsAt() !== null) {
            $field = $propertyPath . '.registration_starts_at';
            $context->setPropertyPath($field);
            $context->addViolation('Se debe especificar a la vez la fecha de inicio y fin', array(), null);            
        }        

        if ($this->getRegistrationStartsAt() > $this->getEndsAt()) {
            $field = $propertyPath . '.registration_starts_at';
            $context->setPropertyPath($field);
            $context->addViolation('El registro no puede empezar después de que la actividad.', array(), null);
        }

        if (null !== $this->getStartsAt() && $this->getStartsAt() < new \DateTime('now')) {
            $field = $propertyPath . '.starts_at';
            $context->setPropertyPath($field);
            $context->addViolation('¿Actividades que empiezan en el pasado? No gracias.', array(), null);
        }
        
    }
}