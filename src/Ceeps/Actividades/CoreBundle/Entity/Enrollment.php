<?php

namespace Ceeps\Actividades\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Ceeps\Actividades\CoreBundle\Entity\Enrollment
 *
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(columns={"user_id", "activity_id"})})
 * @ORM\Entity(repositoryClass="Ceeps\Actividades\CoreBundle\Entity\EnrollmentRepository")
 * @UniqueEntity(fields={"user","activity"})
 */
class Enrollment
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $user
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @var string $activity
     *
     * @ORM\ManyToOne(targetEntity="Activity")
     * @ORM\JoinColumn(nullable=false)
     */
    private $activity;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Assert\DateTime()
     */
    private $created_at;

    /**
     * @var boolean $is_paid
     *
     * @ORM\Column(name="is_paid", type="boolean")
     * @Assert\Type("bool")
     */
    private $is_paid = false;

    /**
     * @var boolean $is_cancelled
     *
     * @ORM\Column(name="is_cancelled", type="boolean")
     * @Assert\Type("bool")
     */
    private $is_cancelled = false;

    /**
     * @var text $comment
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * Get created_at
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set is_paid
     *
     * @param boolean $isPaid
     */
    public function setIsPaid($isPaid)
    {
        $this->is_paid = $isPaid;
    }

    /**
     * Get is_paid
     *
     * @return boolean 
     */
    public function getIsPaid()
    {
        return $this->is_paid;
    }

    /**
     * Set is_cancelled
     *
     * @param boolean $isCancelled
     */
    public function setIsCancelled($isCancelled)
    {
        $this->is_cancelled = $isCancelled;
    }

    /**
     * Get is_cancelled
     *
     * @return boolean 
     */
    public function getIsCancelled()
    {
        return $this->is_cancelled;
    }

    /**
     * Set comment
     *
     * @param text $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * Get comment
     *
     * @return text 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set user
     *
     * @param Ceeps\Actividades\CoreBundle\Entity\User $user
     */
    public function setUser(\Ceeps\Actividades\CoreBundle\Entity\User $user)
    {
        $this->user = $user;
    }

    /**
     * Get user
     *
     * @return Ceeps\Actividades\CoreBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set activity
     *
     * @param Ceeps\Actividades\CoreBundle\Entity\Activity $activity
     */
    public function setActivity(\Ceeps\Actividades\CoreBundle\Entity\Activity $activity)
    {
        $this->activity = $activity;
    }

    /**
     * Get activity
     *
     * @return Ceeps\Actividades\CoreBundle\Entity\Activity 
     */
    public function getActivity()
    {
        return $this->activity;
    }
}